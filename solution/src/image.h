
#include <stdint.h>
#pragma once
struct __attribute__((packed)) pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct __attribute__((packed)) image {
    uint64_t width, height;
    struct pixel *data;
};

struct image image_init(uint32_t height, uint32_t width);


void image_destroy(struct image *image);
//Функция для получения пикселя по его координатам
struct pixel getImage(struct image const *img, uint64_t x, uint64_t y);

//Функция для установки определенногшо пикселя по его координатам
void setImage(struct image *img, uint64_t x, uint64_t y, struct pixel pixel);

