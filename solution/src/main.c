#include "bmp.h"
#include "image.h"
#include "transform.h"

#include <stdio.h>
#include <stdlib.h>

#define ARG_COUNT 4

// Function declarations
void printError(const char *message);
int readImageFromFile(const char *filename, struct image *img);
int writeImageToFile(const char *filename, struct image *img);
void validate_angle(uint32_t angle);

int main(int argc, char **argv) {
    if (argc != ARG_COUNT) {
        printError("Invalid number of arguments");
        fprintf(stderr, "Usage: program input_file output_file rotation_angle\n");
        return 1;
    }

    struct image inputImage = {0};
    if (!readImageFromFile(argv[1], &inputImage)) {
        image_destroy(&inputImage);
        return 1;
    }

    uint32_t angle = (atoi(argv[3]) + 360) % 360;
    validate_angle(angle);

    uint32_t rotations = (4 - angle / 90) % 4;
    for (uint32_t i = 0; i < rotations; i++) {
        struct image rotated = rotation(inputImage);
        image_destroy(&inputImage);
        inputImage = rotated;
    }

    if (!writeImageToFile(argv[2], &inputImage)) {
        image_destroy(&inputImage);
        return 1;
    }
    image_destroy(&inputImage);
    return 0;
}

void validate_angle(uint32_t angle) {
    if (angle % 90 != 0) {
        printError("Invalid angle. Accepted values: 0, 90, 180, 270");
    }
}

void printError(const char *message) {
    fprintf(stderr, "Error: %s\n", message);
}

int readImageFromFile(const char *filename, struct image *img) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        printError("Cannot open file");
        image_destroy(img);
        return 0;
    }

    if (read_from_bmp(file, img) != READ_OK) {
        printError("Error during reading input file");
        fclose(file);
        image_destroy(img);
        return 0;
    }

    fclose(file);
    return 1;
}


int writeImageToFile(const char *filename, struct image *img) {
    FILE *file = fopen(filename, "wb");
    if (file == NULL) {
        printError("Cannot open file for writing");
        image_destroy(img);
        return 0;
    }
    if (write_to_bmp(file, img) != WRITE_OK) {
        printError("Error during writing");
        fclose(file);
        image_destroy(img);
        return 0;
    }
    fclose(file);
    return 1;
}
