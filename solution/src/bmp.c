#include "bmp.h"
#include "image.h"
#include <stdio.h>

const uint16_t BF_TYPE = 19778;
const uint16_t BFBIT_COUNT = 24;
const uint32_t BF_RESERVED = 0;
const uint32_t B_OFF_BITS = 54;
const uint32_t BI_SIZE = 40;
const uint16_t BI_PLANES = 1;
const uint32_t BI_COMPRESSION = 0;
const uint32_t BI_X_PELS_PER_METER = 2834;
const uint32_t BI_Y_PELS_PER_METER = 2834;
const uint32_t BI_CLR_USED = 0;
const uint32_t BI_CLR_IMPORTANT = 0;

static uint32_t calculate_padding(const uint32_t width) {
    return (4 - (width * 3) % 4) % 4;
}

/*
static void print_bmp_header_info(const struct bmp_header *header) {
    printf("bfType: %u\n", header->bfType);
    printf("bfileSize: %u\n", header->bfileSize);
    printf("bfReserved: %u\n", header->bfReserved);
    printf("bOffBits: %u\n", header->bOffBits);
    printf("biSize: %u\n", header->biSize);
    printf("biWidth: %u\n", header->biWidth);
    printf("biHeight: %u\n", header->biHeight);
    printf("biPlanes: %u\n", header->biPlanes);
    printf("biBitCount: %u\n", header->biBitCount);
    printf("biCompression: %u\n", header->biCompression);
    printf("biSizeImage: %u\n", header->biSizeImage);
    printf("biXPelsPerMeter: %u\n", header->biXPelsPerMeter);
    printf("biYPelsPerMeter: %u\n", header->biYPelsPerMeter);
    printf("biClrUsed: %u\n", header->biClrUsed);
    printf("biClrImportant: %u\n", header->biClrImportant);
}
*/

enum read_status read_from_bmp(FILE* in, struct image* img) {
    struct bmp_header bmp;

    if (!fread(&bmp, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_HEADER;
    }

    if (bmp.bfType != BF_TYPE) {
        return READ_INVALID_SIGNATURE;
    }

    if (bmp.biBitCount != BFBIT_COUNT) {
        return READ_INVALID_BITS;
    }

    *img = image_init(bmp.biHeight, bmp.biWidth);
    uint32_t padding = calculate_padding(img->width);

    for (uint32_t i = 0; i < bmp.biHeight; i++) {
        if (fread(img->data + i * bmp.biWidth, sizeof(struct pixel), bmp.biWidth, in) != bmp.biWidth) {
            return READ_INVALID_SIGNATURE;
        }

        if (fseek(in, (long)padding, SEEK_CUR)) {
            return READ_INVALID_SIGNATURE;
        }
    }

    return READ_OK;
}

enum write_status write_to_bmp(FILE* out, const struct image* img) {
    uint32_t padding = calculate_padding(img->width);
    struct bmp_header bmp_header;

    bmp_header.bfType = BF_TYPE;
    bmp_header.biSizeImage = (uint32_t)((sizeof(struct pixel) * img->width + padding) * img->height);
    bmp_header.bfileSize = bmp_header.biSizeImage + sizeof(struct bmp_header);
    bmp_header.bfReserved = BF_RESERVED;
    bmp_header.bOffBits = B_OFF_BITS;
    bmp_header.biSize = BI_SIZE;
    bmp_header.biWidth = (uint32_t)img->width;
    bmp_header.biHeight = (uint32_t)img->height;
    bmp_header.biPlanes = BI_PLANES;
    bmp_header.biBitCount = BFBIT_COUNT;
    bmp_header.biCompression = BI_COMPRESSION;
    bmp_header.biXPelsPerMeter = BI_X_PELS_PER_METER;
    bmp_header.biYPelsPerMeter = BI_Y_PELS_PER_METER;
    bmp_header.biClrUsed = BI_CLR_USED;
    bmp_header.biClrImportant = BI_CLR_IMPORTANT;

    if (fwrite(&bmp_header, sizeof(bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    for (uint32_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * (img->width), 3, img->width, out) != img->width) {
            return WRITE_ERROR;
        }
        uint32_t remains[] = {0, 0, 0, 0};
        fwrite(remains, 1, padding, out);
    }

    return WRITE_OK;
}
