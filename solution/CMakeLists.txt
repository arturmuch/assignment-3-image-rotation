file(GLOB_RECURSE sources CONFIGURE_DEPENDS
    src/*.c
    src/*.h
    include/*.h
)

add_executable(image-transformer ${sources}
        src/bmp.h
        src/image.h
        src/image.c
        src/bmp.c
        src/transform.h
        src/transform.c)
target_include_directories(image-transformer PRIVATE src include)
