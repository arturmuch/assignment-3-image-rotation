#include "image.h"
#include "transform.h"


struct image rotation(struct image source) {
    struct image rotatedImage = image_init( source.width,source.height);
    for (uint32_t i = 0; i < source.height; i++) {
        for (uint32_t j = 0; j < source.width; j++) {
            uint32_t rotatedIndex = source.height * (j + 1) - i - 1;
            uint32_t sourceIndex = source.width * i + j;
            rotatedImage.data[rotatedIndex] = source.data[sourceIndex];
        }
    }
    return rotatedImage;
}
