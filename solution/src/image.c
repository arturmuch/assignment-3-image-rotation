
#include "image.h"
#include <stdint.h>
#include <stdlib.h>

struct image image_init(const uint32_t height, const uint32_t width) {
    return (struct image) {
            .data = malloc(sizeof(struct pixel) * width * height),
            .height = height,
            .width = width
    };
}

void image_destroy(struct image* image) {
    if (image->data == NULL) {
        return;
    }
    free(image->data);
}


struct pixel getImage(struct image const* img, uint64_t x, uint64_t y) {
    if (x >= img->width || y >= img->height) {
        return (struct pixel){
            .b = 0,
            .g = 0,
            .r = 0
        };
    }
    return img->data[img->width * y + x];
}

void setImage(struct image *img, uint64_t x, uint64_t y, struct pixel pixel){
    if (x >= img->width || y >= img->height) {
        return;
    }
    img -> data[img->width*y+x] = pixel;
}
